QT += network core

HEADERS += \
    src/client/clientdefs.h \
    src/client/userinput.h \
    src/client/jobnode.h \
    src/client/pktsendercontrol.h

SOURCES += \
    src/client/userinput.cpp \
    src/client/jobnode.cpp \
    src/client/main.cpp \
    src/client/pktsendercontrol.cpp


INCLUDEPATH += src/dataprocessinglib   \
    include
