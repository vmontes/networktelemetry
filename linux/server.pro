QT += network core

HEADERS += \
    src/server/pktdispatcher.h \
    src/server/dataserver.h

SOURCES += \
    src/server/pktdispatcher.cpp \
    src/server/dataserver.cpp \
    src/server/main.cpp

INCLUDEPATH += include  \
               src/dataprocessinglib

unix:!macx: LIBS += -L$$PWD/../build-dataprocessing-Desktop_Qt_5_14_2_GCC_64bit-Debug/ -ldataprocessing

INCLUDEPATH += $$PWD/..build-dataprocessing-Desktop_Qt_5_14_2_GCC_64bit-Debug
DEPENDPATH += $$PWD/../build-dataprocessing-Desktop_Qt_5_14_2_GCC_64bit-Debug
