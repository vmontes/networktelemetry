/* Rev 27/02/2019 */
#include "binlockresults.h"
#include "binprocessor.h"

CBinLockResults::CBinLockResults()
{
	CBinPktProcessor::results_mtx.lock();
}

CBinLockResults::~CBinLockResults()
{
	CBinPktProcessor::results_mtx.unlock();
}
